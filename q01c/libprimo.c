#include "libprimo.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

int testa_primo(int primo){

    int i;

    // printf("\n --- Going in-testa_primo --- \n");

    if (primo < 2) 
      return FALSE;
    else if (primo < 4) 
      return TRUE;

    if (primo % 2 == 0) 
      return FALSE;

    for(i = 3; i <= sqrt(primo); i+=2)
      if(primo % i == 0) 
        return FALSE;

    // printf("\n --- Going out-testa_primo --- \n");

    return TRUE;
}

int gera_primo(){

  int primo;
  // int count = 0;

  // printf("\n --- Going in-gera_primo --- \n");

  srand((unsigned)time(NULL));

  primo = rand() % 200;
  primo = (primo*primo) + 1;

  while(!testa_primo(primo)) {
    // count++;
    primo++;
  }

  // printf("\n --- Going out-gera_primo --- \n\n");
  // printf("\nQuantidade de incrementos do valor = %d\n", count);

  return primo;
}
