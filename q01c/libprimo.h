#ifndef LIBPRIMO_H
#define LIBPRIMO_H

#define TRUE 1
#define FALSE 0

int testa_primo(int primo);
int gera_primo(void);

#endif
